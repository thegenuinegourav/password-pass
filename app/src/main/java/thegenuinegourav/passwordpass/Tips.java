package thegenuinegourav.passwordpass;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;


public class Tips extends AppCompatActivity {
    private ListView dosList,dontsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);

        dosList=(ListView)findViewById(R.id.dosLayout);
        dontsList=(ListView)findViewById(R.id.dontsLayout);

        dosList.setTextFilterEnabled(true);
        LayoutAnimationController controller
                = AnimationUtils.loadLayoutAnimation(
                this, R.anim.controller);
        dosList.setLayoutAnimation(controller);

        dontsList.setTextFilterEnabled(true);
        dontsList.setLayoutAnimation(controller);
    }
}
