package thegenuinegourav.passwordpass;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;




public class MainActivity extends Activity {

    private EditText name,value;
    private String Name,pass,WhichActivity;
    private int N,length;
    private long sum,password;
    private Button button,button2;
    private TextView NumberText;
    private Intent in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NumberText = (TextView)findViewById(R.id.textView4);
        in =new Intent(getApplicationContext(),MainActivity2.class);

        WhichActivity = getIntent().getExtras().getString("WhichActivity");
        if(WhichActivity.equals("Create")) {
            NumberText.setText("Number You Want to Remember");
        }else {
            NumberText.setText("Number You have Remembered");
        }

        name = (EditText)findViewById(R.id.name);
        value = (EditText)findViewById(R.id.value);
        button = (Button)findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button8);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Name = name.getText().toString().trim();
                length=0;sum=0;password=0;N=0; //Initializing all values to 0
                if(Name.length()<1) System.out.println("Empty String");
                else {
                    while (length<Name.length())  //Calculation of space
                    {
                        if (Name.charAt(length)==' ') {length++;break;}
                        length++;
                    }
                }
                try {
                    N = Integer.parseInt(value.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }

                if(value.getText().toString().length()>9) Toast.makeText(getApplicationContext(),"Too large value of N! Make it small",Toast.LENGTH_SHORT).show();
                else if(length==0) Toast.makeText(getApplicationContext(),"Please Complete the entries first",Toast.LENGTH_SHORT).show();
                else if(N==0) Toast.makeText(getApplicationContext(),"Please Complete the entries first",Toast.LENGTH_SHORT).show();
                else if(N>=100000000) Toast.makeText(getApplicationContext(),"Too many digits! Make it small",Toast.LENGTH_SHORT).show();
                else if(N==1000 || N==10000 || N==100000 || N==1000000 || N==10000000) Toast.makeText(getApplicationContext(),"Try to avoid multiple of 10 values",Toast.LENGTH_SHORT).show();
                else
                {
                    for (int i=0;i<Name.length();i++)  //Calculation of password
                    {
                        char character = Name.charAt(i);
                        sum = sum + (int)character;
                    }
                    sum = sum % 1000000;
                    password = sum*N*N;
                    password = password % 1000000;

                    pass = String.valueOf(password);
                    switch (String.valueOf(password).length())
                    {
                        case 0: pass = pass + "000000"; break;
                        case 1: pass = "00" + pass + "000"; break;
                        case 2: pass = "00" + pass + "00"; break;
                        case 3: pass = "0" + pass + "00"; break;
                        case 4: pass = "0" + pass + "0"; break;
                        case 5: pass = pass + "0"; break;
                        default:break;
                    }
                    Intent i =new Intent(MainActivity.this,MainActivity2.class);
                    if(WhichActivity.equals("Create")) {
                        i.putExtra("WhichActivity","Create");
                    }else {
                        i.putExtra("WhichActivity","Access");
                    }
                    i.putExtra("MyTag",pass);
                    i.putExtra("Name",Name);
                    i.putExtra("ValueOfN",String.valueOf(N));
                    startActivity(i);
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Name = name.getText().toString().trim();
                length=0;sum=0;password=0;N=0; //Initializing all values to 0
                if(Name.length()<1) System.out.println("Empty String");
                else {
                    while (length<Name.length())  //Calculation of space
                    {
                        if (Name.charAt(length)==' ') {length++;break;}
                        length++;
                    }
                }
                try {
                    N = Integer.parseInt(value.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }

                if(value.getText().toString().length()>9) Toast.makeText(getApplicationContext(),"Too large value of N! Make it small",Toast.LENGTH_SHORT).show();
                else if(length==0) Toast.makeText(getApplicationContext(),"Please Complete the entries first",Toast.LENGTH_SHORT).show();
                else if(N==0) Toast.makeText(getApplicationContext(),"Please Complete the entries first",Toast.LENGTH_SHORT).show();
                else if(N>=100000000) Toast.makeText(getApplicationContext(),"Too many digits! Make it small",Toast.LENGTH_SHORT).show();
                else if(N==1000 || N==10000 || N==100000 || N==1000000 || N==10000000) Toast.makeText(getApplicationContext(),"Try to avoid multiple of 10 values",Toast.LENGTH_SHORT).show();
                else
                {
                    for (int i=0;i<Name.length();i++)  //Calculation of password
                    {
                        char character = Name.charAt(i);
                        sum = sum + (int)character;
                    }
                    sum = sum % 1000000;
                    password = sum*N*N;
                    password = password % 1000000;
                    pass = String.valueOf(password);
                    String sb;
                    if(Name.length()>=4)
                    sb=String.valueOf(Name.charAt(0)) + String.valueOf(Name.charAt(Name.length()-1)) + String.valueOf(Name.charAt(Name.length()-2)) + String.valueOf(Name.charAt(Name.length()-3)) + String.valueOf(Name.charAt(Name.length()-4));
                     else{
                        StringBuffer buffer = new StringBuffer(Name);
                        buffer.reverse();
                        sb = buffer.toString();
                    }

                    for(int i=0;i<pass.length();i++)
                    {
                        switch(String.valueOf(pass.charAt(i)))
                        {
                            case "0": sb=sb + "&"; break;
                            case "1": sb=sb+"#";break;
                            case "2": sb=sb+String.valueOf(Name.charAt(Name.length()-1)); break;
                            default: String s=String.valueOf(pass.charAt(i)) + String.valueOf(i);
                                    int n=0;
                                    try{
                                        n=Integer.parseInt(s);
                                    }catch (NumberFormatException ne)
                                    {
                                        System.out.println("Could not convert " + ne);
                                    }
                                   char c= (char)n;
                                   sb = sb +String.valueOf(c);
                        }
                    }
                    Intent i =new Intent(MainActivity.this,MainActivity2.class);
                    if(WhichActivity.equals("Create")) {
                        i.putExtra("WhichActivity","Create");
                    }else {
                        i.putExtra("WhichActivity","Access");
                    }
                    i.putExtra("MyTag2",sb);
                    i.putExtra("Name",Name);
                    i.putExtra("ValueOfN",String.valueOf(N));
                    startActivity(i);
                }
            }
        });
    }

    public void info(View view)
    {
        Intent intent =new Intent(getApplicationContext(),InfoActivity.class);
        startActivity(intent);
    }

}
