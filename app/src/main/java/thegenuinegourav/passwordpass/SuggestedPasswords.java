package thegenuinegourav.passwordpass;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.ClipboardManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;

public class SuggestedPasswords extends Activity {
    private ListView listView;
    private String history;
    private TextView textView;
    private ArrayList<String> passwdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggested_passwords);

        listView = (ListView)findViewById(R.id.listView);
        passwdList=getIntent().getExtras().getStringArrayList("PasswordsList");

        String[] stockArr = new String[passwdList.size()];
        stockArr = passwdList.toArray(stockArr);

        listView.setAdapter(new CardCustomAdapter(this, stockArr));

        listView.setTextFilterEnabled(true);

        LayoutAnimationController controller
                = AnimationUtils.loadLayoutAnimation(
                this, R.anim.list_layout_controller);
        listView.setLayoutAnimation(controller);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        history = "PetName + SurName + has + (Initial 4 numbers from your phone number) + initialNo."; showCustomAlert();
                        break;
                    case 1:
                        history = "Reverse of FirstName + Is + FirstName + From + Year"; showCustomAlert();
                        break;
                    case 2:
                        history = "PNFOR(stands for PetName For) + FirstName + SurName + IS + PetName + !"; showCustomAlert();
                        break;
                    case 3:
                        history = "Reverse of Phone Number + is + reverse + PhoneNo."; showCustomAlert();
                        break;
                    case 4:
                        history = "Month + likes + Date + Since + Year"; showCustomAlert();
                        break;
                    case 5:
                        history = "FirstName + Phone Number + SurName + ?"; showCustomAlert();
                        break;
                    case 6:
                        history = "strcmp(\"PetName\",\"FirstName\") + always + Gives + True"; showCustomAlert();
                        break;
                    case 7:
                        history = "FirstName + Year + SurName"; showCustomAlert();
                        break;
                    case 8:
                        history = "Your Mobile Number upto 8 digits with spacing between each two"; showCustomAlert();
                        break;
                    case 9:
                        history = "PetName + Initial of you LastName + wasbornON + Year,Date/Month"; showCustomAlert();
                        break;
                    case 10:
                        history = "WMO (stands for Wish Me On) + Date,Year,Month"; showCustomAlert();
                        break;
                    default:
                        break;
                }
            }
        });



        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ((ClipboardManager)getSystemService(CLIPBOARD_SERVICE)).setText(passwdList.get(position));
                Toast.makeText(getApplicationContext(), "Username Copied!",
                        Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }

    public void showCustomAlert()
    {


        Context context = getApplicationContext();
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();

        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.custom_toast, null);

        Toast toast = new Toast(context);

        textView = (TextView)toastRoot.findViewById(R.id.toastToBeDisplayed);
        textView.setText(history);

        // Set layout to toast
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
                0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();

    }
}
