package thegenuinegourav.passwordpass;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class InfoActivity extends AppCompatActivity {

    private ListView listView;
    private String[] HeadingNames={"What is Password Pass?","How it works?","How to use?","How to use it to the best?","IMPORTANT Facts to be noted","Security?"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(new CustomAdapter(this, HeadingNames));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            /*
             * Parameters:
               adapter - The AdapterView where the click happened.
               view - The view within the AdapterView that was clicked
               position - The position of the view in the adapter.
               id - The row id of the item that was clicked.
             */
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView itemSelected = (TextView) view.findViewById(R.id.text1);
                RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layoutConatainingTextView);
                layout.getLayoutParams().height= RelativeLayout.LayoutParams.WRAP_CONTENT;

                switch (position) {
                    case 0: if(itemSelected.getText().equals(HeadingNames[0])) itemSelected.setText("\n" + itemSelected.getText().toString() + "\n\n" + getString(R.string.answer_to_what_is_whaff));
                            else {
                        itemSelected.setText(HeadingNames[0]);
                        layout.getLayoutParams().height= RelativeLayout.LayoutParams.MATCH_PARENT;
                    }
                            break;

                    case 1: if(itemSelected.getText().equals(HeadingNames[1])) itemSelected.setText("\n" + itemSelected.getText().toString() + "\n\n" + getString(R.string.answer_to_how_it_works));
                    else {
                        itemSelected.setText(HeadingNames[1]);
                        layout.getLayoutParams().height= RelativeLayout.LayoutParams.MATCH_PARENT;
                    }
                        break;
                    case 2: if(itemSelected.getText().equals(HeadingNames[2])) itemSelected.setText("\n" + itemSelected.getText().toString() + "\n\n" + getString(R.string.answer_to_how_to_use));
                    else {
                        itemSelected.setText(HeadingNames[2]);
                        layout.getLayoutParams().height= RelativeLayout.LayoutParams.MATCH_PARENT;
                    }
                        break;
                    case 3: if(itemSelected.getText().equals(HeadingNames[3])) itemSelected.setText("\n" + itemSelected.getText().toString() + "\n\n" + getString(R.string.answer_to_use_it_to_the_best));
                    else {
                        itemSelected.setText(HeadingNames[3]);
                        layout.getLayoutParams().height= RelativeLayout.LayoutParams.MATCH_PARENT;
                    }
                        break;

                    case 4: if(itemSelected.getText().equals(HeadingNames[4])) itemSelected.setText("\n" + itemSelected.getText().toString() + "\n\n" + getString(R.string.answer_to_important_facts_to_be_noted));
                    else {
                        itemSelected.setText(HeadingNames[4]);
                        layout.getLayoutParams().height= RelativeLayout.LayoutParams.MATCH_PARENT;
                    }
                        break;

                    case 5: if(itemSelected.getText().equals(HeadingNames[5])) itemSelected.setText("\n" + itemSelected.getText().toString() + "\n\n" + getString(R.string.answer_to_security));
                    else {
                        itemSelected.setText(HeadingNames[5]);
                        layout.getLayoutParams().height= RelativeLayout.LayoutParams.MATCH_PARENT;
                    }
                        break;
                }

            }
        });
    }

}
