package thegenuinegourav.passwordpass;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HomeActivity extends Activity {

    private String colorSaved;
    private Bundle bundle;
    private Button savedColorText;
    private RelativeLayout layoutOfSavedColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        savedColorText = (Button)findViewById(R.id.SavedColor);
        layoutOfSavedColor = (RelativeLayout)findViewById(R.id.layoutOfSavedColor);

        bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            layoutOfSavedColor.setVisibility(View.VISIBLE);
            colorSaved = bundle.getString("ColorSaved");
            savedColorText.setText("Saved Color  :  " + colorSaved);
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            layoutOfSavedColor.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            };
            thread.start(); //start the thread
        }


    }

    public void Create(View view)
    {
        Intent i = new Intent(HomeActivity.this,MainActivity.class);
        i.putExtra("WhichActivity","Create");
        startActivity(i);
    }

    public void Access(View view)
    {
        Intent i = new Intent(HomeActivity.this,MainActivity.class);
        i.putExtra("WhichActivity","Access");
        startActivity(i);
    }

    public void Suggest(View view)
    {
        Intent i = new Intent(HomeActivity.this,DetailsActivity.class);
        startActivity(i);
    }
    public void info(View view)
    {
        Intent i =new Intent(HomeActivity.this,InfoActivity.class);
        startActivity(i);
    }
    public void Tips(View vew)
    {
        Intent i =new Intent(HomeActivity.this,Tips.class);
        startActivity(i);
    }


    public void ViewPassword(View view) {
        final Intent intentColorPickerActivity = new Intent(this, ColorPickerActivity.class);
        intentColorPickerActivity.putExtra("ViewPassword",true);
        startActivity(intentColorPickerActivity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAffinity(this);
        System.exit(0);
        finish();
    }
}
