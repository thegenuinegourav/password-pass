package thegenuinegourav.passwordpass;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends ArrayAdapter<String> {
    CustomAdapter(Context context,String[] Head)
    {
        super(context,R.layout.custom_listview,Head);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view=inflater.inflate(R.layout.custom_listview, parent, false);
        final TextView text1;
        text1 = (TextView)view.findViewById(R.id.text1);
        text1.setText(getItem(position));
        return view;
    }
}
