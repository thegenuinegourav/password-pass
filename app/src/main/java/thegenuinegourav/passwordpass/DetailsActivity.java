package thegenuinegourav.passwordpass;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class DetailsActivity extends Activity {

    private TextInputLayout first_name, last_name, pet_name, mobile;
    private String First, Last, Pet, Mobile, Date, Month, Year;
    private ArrayList<String> array;
    private Button button;
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        array = new ArrayList<String>();
        button = (Button) findViewById(R.id.SubmitDetails);

        first_name = (TextInputLayout) findViewById(R.id.firstnameWrapper);
        last_name = (TextInputLayout) findViewById(R.id.lastnameWrapper);
        pet_name = (TextInputLayout) findViewById(R.id.petnameWrapper);
        mobile = (TextInputLayout) findViewById(R.id.mobileWrapper);
        dateView = (TextView) findViewById(R.id.textView3);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);

        first_name.setHint("First Name");
        last_name.setHint("Last Name");
        pet_name.setHint("Pet Name");
        mobile.setHint("Phone Number");


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                First = first_name.getEditText().getText().toString();
                Last = last_name.getEditText().getText().toString();
                Pet = pet_name.getEditText().getText().toString();
                Mobile = mobile.getEditText().getText().toString();

                //Removing unnecessary spaces
                First = First.replace(" ","");
                Last = Last.replace(" ","");
                Pet = Pet.replace(" ","");

                //Removing trailing spaces
                Mobile=Mobile.replaceFirst("\\s+$", "");
                Date = String.valueOf(day);
                Month = String.valueOf(month);
                Year = String.valueOf(year);

                if (First.matches("") || Last.matches("") || Pet.matches("") || Mobile.matches("") || Date.matches("") || Month.matches("") || Year.matches(""))
                    Toast.makeText(getApplicationContext(), "Please Complete the entries first and then click", Toast.LENGTH_SHORT).show();
                else if (Mobile.length() != 10 || Mobile.length() < 0)
                    Toast.makeText(getApplicationContext(), "Invalid Mobile Number", Toast.LENGTH_SHORT).show();
                else {
                    array = calculatePasswords(First, Last, Pet, Mobile, Date, Month, Year);
                    Intent i = new Intent(getApplicationContext(), SuggestedPasswords.class);
                    i.putExtra("PasswordsList", array);
                    System.out.print("Gonna Started");
                    startActivity(i);
                    System.out.print("Stopped");
                }

            }
        });
    }

    public ArrayList<String> calculatePasswords(String First, String Last, String Pet, String Mobile, String Date, String Month, String Year) {
        //For 1st password
        String FirstPasswd = Pet + Last + "has" + Mobile.substring(0, 4) + "initialNo.";

        //For 2nd passord
        String SecondPasswd = (new StringBuffer(First).reverse().toString()) + "Is" + First + "From" + Year;

        //For 3rd password
        String ThirdPasswd = "PNFOR" + First + Last + "IS" + Pet + "!";

        //For 4th passwd
        String FourthPassed = (new StringBuffer(Mobile).reverse().toString()) + "isReversePhoneNo.";

        //For 51h password
        String FifthPassword = Month + "likes" + Date + "Since" + Year;

        //For 6th password
        String SixthPasswd = First + Mobile + Last + "?";

        //For 7th password
        String SeventhPasswd = "strcmp(\"" + Pet + "\",\"" + First + "\")alwaysGivesTrue";

        //For 8th password
        String EightPasswd = First + Year + Last;

        //9th
        String NinthPasswd = Mobile.substring(0,1) + " " + Mobile.substring(1,2) + " " + Mobile.substring(2,3) + " " +
                Mobile.substring(3,4) + " " + Mobile.substring(4,5) + " " + Mobile.substring(5,6) + " " + Mobile.substring(6,7) + " " +
                Mobile.substring(7,8);

        //10th
        String TenthPasswd = Pet + String.valueOf(Last.charAt(0)) + "wasbornON" + Year + "," + Date + "/" +Month;

        //11th
        String EleventhPasswd = "WMO " + Date + "," + Year + "," + Month;

        ArrayList<String> myArray = new ArrayList<String>();
        myArray.add(FirstPasswd);
        myArray.add(SecondPasswd);
        myArray.add(ThirdPasswd);
        myArray.add(FourthPassed);
        myArray.add(FifthPassword);
        myArray.add(SixthPasswd);
        myArray.add(SeventhPasswd);
        myArray.add(EightPasswd);
        myArray.add(NinthPasswd);
        myArray.add(TenthPasswd);
        myArray.add(EleventhPasswd);
        return myArray;
    }


    public void Back(View view) {
        finish();
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
        this.day = day;
        this.month = month;
        this.year = year;
    }
}
