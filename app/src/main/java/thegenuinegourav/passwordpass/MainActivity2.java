package thegenuinegourav.passwordpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends Activity {
    private TextView textView;
    private String password=null,wpassword=null,Name,N;
    private String WhichActivty;
    private Button HideButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        password=null;
        wpassword=null;

        HideButton = (Button)findViewById(R.id.HideButton);

        WhichActivty = getIntent().getExtras().getString("WhichActivity");
        if(WhichActivty.equals("Create"))
        {
            HideButton.setVisibility(View.VISIBLE);
        }else{
            HideButton.setVisibility(View.GONE);
        }
        textView = (TextView)findViewById(R.id.textView2);
        password=getIntent().getExtras().getString("MyTag");
        wpassword=getIntent().getExtras().getString("MyTag2");
        Name = getIntent().getExtras().getString("Name");
        N= getIntent().getExtras().getString("ValueOfN");

        if(wpassword!=null)
        {
            textView.setTextSize(40);
            textView.setText(wpassword);

        }else{
            textView.setText(password);
        }
    }

    public void HidePasswordInColor(View view) {
        final Intent intentColorPickerActivity = new Intent(this, ColorPickerActivity.class);
        intentColorPickerActivity.putExtra("ViewPassword",false);
        intentColorPickerActivity.putExtra("Password",textView.getText().toString());
        startActivity(intentColorPickerActivity);
    }
}
